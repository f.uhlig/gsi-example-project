# GSI example project

This is an example project which shows the the propossed files when starting
a new project. Beside a README.md which describes the project and how to
build it, the following files should be also included.

## License information

The repository should contain a file LICENSE with the full license text. The
information about licensing by GSI can be found at ...
Normally a free software license should be used. For this example project
the standard licence at GSI GPLv3 was chosen.

## Naming and Formating Guidelines

The repository should contain a file or files describing the naming and
formating conventions of the project such that new users can easily start
contributing. For the formatting it is proposed to use a tool
such as clang-tidy to automatically format the code correctly. In this case
the file describing the format is the format definition file used by the
tool. Since naming and formating are very project specific no files
describing the naming and the formating are added to this example project.

## Code of Conduct

It is strongly recommended to add a code of conduct file which describes the
proper behaving of the participants when interacting with each other. An
example of such a file is added to the example project 

## Contributing

When outside users start contributing to the project it is needed that they
know how this contribution is expected. To clearly define this an example 
CONTRIBUTING.md file can be found in the example project. When adding code
to the project it is neccesary that developers confirm that the added code
is their intelectual property. This is done accepting the DCO.

### Developer Certificate of Origin (DCO)

Any code contributions going into this example project will become part of a GPL-licensed, open source repository.
It is therefore mendatory that code submissions belong to the authors, and that submitters have the authority to
merge that code into the public codebase of the project.

For that purpose, one should use a Developer's Certificate of Origin. It is the same document used by other projects.
Signing the DCO states that there are no legal reasons to not merge the code.

## Build and test the project

To build the project the tool CMake is needed. If the tool isn't available
it can be downloaded from https://cmake.org/download/. Further information
about the tool can be found on https://cmake.org. 

To generate the needed build files the following command needs to be
executed from the source directory.

cmake -S . -B build

which creates a build directory with the needed files. To compile the code
use

cmake --build build

and to test the code use

cmake --build build --target test
