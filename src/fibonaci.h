// function to calculate the fibonaci numbers

int fibonaci(int n) 
{     
    // Create an array to hold all
    // calculated fibonaci numbers
    // An extra element is needed to
    // handle the case n=0
    int fib[n + 2];  
    int i; 
  
    // The first two values of the
    // fibonaci numbers are defined
    // to be 0 and 1.
    fib[0] = 0; 
    fib[1] = 1; 
  
    // start calculation only from
    // two, since the values for 0 
    // and one are known.
    for(i = 2; i <= n; ++i) 
    { 
          
       // The fibonaci value of a given value
       // is the sum of the two previous values
       fib[i] = fib[i - 1] + fib[i - 2]; 
    } 
    return fib[n]; 
}; 
