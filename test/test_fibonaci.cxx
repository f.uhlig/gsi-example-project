// Simple test which checks if the function returns the first
// 13 fibonaci numbers (0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144)
// correctly.

#include "fibonaci.h"

#include <cassert>
#include <iostream>

int main()
{

  constexpr int valuesToTest{13};
  int expected_values[valuesToTest]{0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144};

  for (int val=0; val < valuesToTest; ++val) {
    assert( (fibonaci(val) == expected_values[val]) && "Test failed.");
  }

  std::cout << "All tests were successfull.\n";

  return 0;
}